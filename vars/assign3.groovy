def call(dockerRepoName, imageName, portNum) {
  pipeline {
      agent any
      stages {
            stage('Build') {
                steps {
                    sh 'pip install -r requirements.txt'
                }
            }
            stage('Lint') {
                steps {
                    sh 'pylint-fail-under --fail_under 5 *.py'
                }
            }
            stage('Security Scan') {
                steps {
                    sh 'pip-audit -r ./requirements.txt'
                }
            }
            stage('Package') {
                steps {
                    withCredentials([string(credentialsId: 'dockerhub-daryush', variable: 'TOKEN')]) {
                        sh "docker login -u 'dbalsara2003' -p '$TOKEN' docker.io"
                        sh "docker build -t ${dockerRepoName}:latest --tag dbalsara2003/${dockerRepoName}:${imageName} ."
                        sh "docker push dbalsara2003/${dockerRepoName}:${imageName}"

                    }
                }
            }
            stage('Deploy') {
                steps {
                    withCredentials([string(credentialsId: 'dockerhub-daryush', variable: 'TOKEN')]){
                        sshagent(credentials : ['ssh_key']) {
                            sh "ssh -o StrictHostKeyChecking=no root@159.89.113.215 docker login -u 'dbalsara2003' -p '$TOKEN' docker.io"
                            sh "ssh -o StrictHostKeyChecking=no root@159.89.113.215 docker pull dbalsara2003/${dockerRepoName}:${imageName}"
                            sh "ssh -o StrictHostKeyChecking=no root@159.89.113.215 docker-compose -f /root/assign3/docker-compose.yml up -d "
                        }
                    }
                }
            }
        }
    }
}

